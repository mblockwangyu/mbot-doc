# Summary

## 帮助文档

* [mBot](README.md)
    * [简介](tutorials/introduction.md)
    * [搭建mBot](tutorials/building.md)
    * [预设模式](tutorials/preset-modes.md)
    * [连接mBot](tutorials/connect.md)
    * [快速上手](tutorials/quick-start.md)
* [积木说明](block-reference/block-reference.md)
    * [显示](block-reference/looks.md)
    * [声光](block-reference/show.md)
    * [运动](block-reference/Action.md)
    * [感知](block-reference/sensing.md)
    * [事件](block-reference/events.md)
    * [控制](block-reference/control.md)
    * [运算](block-reference/operators.md)
* [mBot 扩展玩法](expand-mbot/expand-mbot.md)
* [mBot 相关套装](mbot-kits/mbot-kits.md)
* [FAQ](faq/faq.md)