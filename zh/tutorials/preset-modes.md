# 预设模式

mBot出厂预设了三种畅玩模式：
- [超声波避障模式](#超声波避障模式)
- [红外遥控模式](#红外遥控模式)
- [巡线模式](#巡线模式)

通过<span style="color:lightblue;"><b>红外遥控器或板载按钮</b></span>可以更换模式。

<img src="images/modes.png" style="padding:5px 5px 15px 0px;">

<div margin="10px;" style="background-color:#E4F3F7;color:dark-grey;padding:20px;" width="60%;">
根据主板上的LED彩灯可以判断当前模式：<br>
<span style="padding:2px;"><img src="../../en/tutorials/images/led-1.png" width="20px"></span> 白色：红外遥控模式<br>
<span style="padding:2px;"><img src="../../en/tutorials/images/led-2.png" width="20px"></span> 绿色：超声波避障模式<br>
<span style="padding:2px;"><img src="../../en/tutorials/images/led-3.png" width="20px"></span> 蓝色：巡线模式<br>
</div>

<br>

## 超声波避障模式

在超声波避障模式下，mBot可以自动行走并躲避障碍物。

<img src="images/modes-2.png" style="padding:5px 5px 15px 5px;">

## 红外遥控模式

套装内配套了专用的红外遥控器，可以控制mBot的运动方向和速度。

**注：建议将mBot放置在平整的地面。**

<img src="images/modes-3.png" style="padding:5px 5px 15px 5px;">

## 巡线模式

在巡线模式下，mBot可以自动跟随地图上的黑线移动。

<img src="images/modes-4.png" style="padding:5px 5px 15px 5px;">