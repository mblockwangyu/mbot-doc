# 快速上手

让我们从一个简单的项目开始吧！让mBot的LED灯交替闪烁红色和黄色。

<img src="../../en/tutorials/images/get-started-5.gif" width="250px;" style="padding:5px 5px 20px 5px;">

### 连接mBot

1\. 点击“连接”将mBot连接到慧编程。

<img src="images/get-started-1.gif" width="800px;" style="padding:5px 5px 20px 5px;">

### 开始编程

2\. 拖取两个声光类积木 <span style="background-color:#9013FE;color:white;padding:3px; font-size: 12px">亮起（全部）灯，颜色为（），持续（）秒</span> 到脚本区，并将第二块积木的颜色改为黄色。

<img src="images/get-started-2.gif" width="800px;" style="padding:5px 5px 20px 5px;">

3\. 添加一个控制类积木 <span style="background-color:#FFAB19;color:white;padding:3px; font-size: 12px">重复执行（10）次</span> 将两个声光类积木包裹起来。

<img src="images/get-started-3.gif" width="800px;" style="padding:5px 5px 20px 5px;">

4\. 添加一个事件类积木 <span style="background-color:#FFBF00;color:white;padding:3px; font-size: 12px">当绿色旗帜被点击</span> 并放置在所有积木上方。

<img src="images/get-started-4.gif" width="800px;" style="padding:5px 5px 20px 5px;">

5\. 点击舞台下方的绿色旗帜看看吧！

<img src="../../en/tutorials/images/get-started-6.png" width="300px;" style="padding:5px 5px 20px 5px;">