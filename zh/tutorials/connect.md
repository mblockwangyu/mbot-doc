# 连接mBot

可以通过 USB 线、蓝牙 4.0 或 2.4G 模块将 mBot 连接到慧编程。

## 方法一：通过 USB 线

1\. 使用包装提供的 USB 数据线将 mBot 连接到电脑的 USB 口。

2\. 将 mBot 开机。

<img src="images/connect-mbot-1.png" width="300px;" style="padding:5px 5px 20px 5px;">

3\. 在“设备”下，点击“+”，在弹出的设备库窗口选择“mBot”并点击“确定”。

<img src="images/connect-mbot-2.png" width="300px;" style="padding:5px;"><span style="font-size:30px;color:lightblue;padding:10px;">&rarr;</span><img src="images/connect-mbot-3.png" style="padding:5px;">

4\. 点击“连接”。在弹出的连接设备窗口，选择“USB”，并点击“连接”。

<img src="images/connect-mbot-4.png" width="300px;" style="padding:5px;"><span style="font-size:30px;color:lightblue;padding:10px;">&rarr;</span> <img src="images/connect-mbot-5.png" width="250px;" style="padding:5px;">

## 方法二：通过蓝牙4.0

通过蓝牙 4.0 可以将 mBot 连接到电脑。系统要求如下：
- **Windows**：内置蓝牙协议需为蓝牙 4.0；若为其它版本的蓝牙协议，建议购买蓝牙4.0适配器（详细指导请见[蓝牙 4.0指南](http://www.mblock.cc/doc/zh/part-one-basics/connect-devices.html#3-Windows用户蓝牙4.0指南)）
- **Mac OS**：支持大部分型号

1\. 将 mBot 开机。

<img src="images/connect-mbot-1.png" width="300px;" style="padding:5px 5px 20px 5px;">

2\. 启用电脑蓝牙。
- Windows: 在任务栏，依次选择**操作中心** > **蓝牙**
- Mac OS: 依次选择**苹果菜单** > **系统偏好设置**，然后点按**蓝牙**

3\. 在“设备”下，点击“+”，在弹出的设备库窗口选择“mBot”并点击“确定”。

<img src="images/connect-mbot-2.png" width="300px;" style="padding:5px;"><span style="font-size:30px;color:lightblue;padding:10px;">&rarr;</span> <img src="images/connect-mbot-3.png" style="padding:5px;">

4\. 点击“连接”。在弹出的连接设备窗口，选择“蓝牙4.0”，并点击“连接”。

<img src="images/connect-mbot-4.png" width="300px;" style="padding:5px;"><span style="font-size:30px;color:lightblue;padding:10px;">&rarr;</span> <img src="images/connect-mbot-6.png" width="250px;" style="padding:5px;">

## 方法三：通过 2.4G 模块

通过 2.4G 模块可以将 mBot 连接到电脑。
电脑系统建议如下：
- **Windows**：Windows 7 或以上版本
- **Mac OS**：macOS Sierra 10.12 或以上版本

连接时需要使用 2.4G 模块及 2.4G 适配器：

<img src="images/connect-mbot-7.png" width="300px;" style="padding:5px;"><span style="font-size:30px;color:lightblue;padding:10px;">

即插即用，无需驱动或配对。


1\. 将两个部件分别插入 mBot 的 mCore 主板和电脑 USB 接口。


2\. 将 mBot 开机。

<img src="images/connect-mbot-1.png" width="300px;" style="padding:5px 5px 20px 5px;">

3\. 在“设备”下，点击“+”，在弹出的设备库窗口选择“mBot”并点击“确定”。

<img src="images/connect-mbot-2.png" width="300px;" style="padding:5px;"><span style="font-size:30px;color:lightblue;padding:10px;">&rarr;</span><img src="images/connect-mbot-3.png" style="padding:5px;">

4\. 点击“连接”。在弹出的连接设备窗口，选择“2.4G”，并点击“连接”。

<img src="images/connect-mbot-4.png" width="300px;" style="padding:5px;"><span style="font-size:30px;color:lightblue;padding:10px;">&rarr;</span> <img src="images/connect-mbot-8.png" width="250px;" style="padding:5px;">

连接成功后，慧编程将显示 2.4G 连接信息。

<img src="images/connect-mbot-9.png" width="300px;" style="padding:5px;">

<div style="background-color:#EAF6FD;padding:10px;border-left:5px solid #BAE3F9;font-size: 14px;line-height:24px;margin-bottom:20px;border-radius:5px;"><strong>注：</strong><br>mBot 无法通过 2.4G 连接方式更新固件。<br>如果在使用慧编程对 mBot 进行编程的过程中，慧编程提示“更新固件”，则需先退出 2.4G 连接，使用 USB 方式连接 mBot 进行固件更新，固件更新成功后再使用 2.4G 方式连接。
</div>