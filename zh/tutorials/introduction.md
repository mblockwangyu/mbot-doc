<img src="../faq/images/mbot.jpg" width="200px;" style="padding:10px 10px 20px;">

# 简介

作为入门级的STEAM教育机器人，mBot让机器人编程学习和教学变得简单有趣。只需一把螺丝刀，一份入门指南，一节课的时间，孩子就能从零开始，体验动手创造的乐趣，认识各种机器人机械和电子零件，入门学习积木式编程，并锻炼逻辑和设计思维。

## 使用慧编程

结合慧编程软件，你可以通过编程让mBot做各种有趣的事。

<img src="../../en/tutorials/images/mblock5.png" width="400px;" style="padding: 5px 5px 15px 0px;">

慧编程软件有电脑端和移动端，也可以在网页上访问。

- 电脑端：[http://www.mblock.cc/mblock-software/](http://www.mblock.cc/mblock-software/)<br/>
- 安卓和iOS：请在应用商店搜索“慧编程”下载使用<br/>
- 网页端：[https://ide.makeblock.com/](https://ide.makeblock.com/)

**_注意：_**_此帮助文档的截图都为慧编程电脑端，仅供参考。但所有的示例在电脑端和移动端都能实现。_