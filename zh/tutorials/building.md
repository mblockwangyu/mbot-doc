# 搭建mBot

- [零件清单](#零件清单)
- [学习使用螺丝刀](#学习使用螺丝刀)
- [认识主板](#认识主板)
- [搭建指导](#搭建指导)

## 零件清单

<img src="images/parts-list.png" width="800px;" style="padding: 5px 5px 15px 0px;">

## 学习使用螺丝刀

<img src="images/screw-driver.png" style="padding: 5px 5px 15px 0px;">

## 认识主板

<img src="images/main-board.png" style="padding: 5px 5px 15px 0px;">

## 搭建指导

<img src="images/building-1.png" style="padding: 5px 5px 15px 0px;">

<img src="images/building-2.png" style="padding: 5px 5px 15px 0px;">

<img src="images/building-3.png" style="padding: 5px 5px 15px 0px;">

<img src="images/building-4.png" style="padding: 5px 5px 15px 0px;">

<img src="images/building-5.png" style="padding: 5px 5px 15px 0px;">

<img src="images/building-6.png" style="padding: 5px 5px 15px 0px;">

<img src="images/building-7.png" style="padding: 5px 5px 15px 0px;">