# mBot 扩展玩法
你可以给 mBot 加上你想要的零件，对 mBot 进行变身大改造！例如把 mBot 变成跳舞的小猫、疯狂小青蛙等。Makeblock 提供了非常丰富的扩展包，如六足连杆机器人扩展包、声光互动扩展包以及动感小猫扩展包等，更多酷炫造型等你来创造。
## 扩展包说明书下载
<table  cellpadding="15px;" cellspacing="12px;" style="font-size: 12px;text-align:center;">
<tr>
<td width="25%;"><img src="images/six-legged.png" width="150px;"></td>
<td width="25%;"><img src="images/servo-pack.png" width="150px;"></td>
<td width="25%;"><img src="images/interactive-light-sound.png" width="150px;"></td>
</tr>
<tr>
<td width="25%;"><a href="manual/六足机器人.rar" download style="font-weight:bold;">六足机器人扩展包</a></td>
<td width="25%;"><a href="manual/动感小猫.rar" download style="font-weight:bold;">动感小猫扩展包</a></td>
<td width="25%;"><a href="manual/声光互动.rar" download style="font-weight:bold;">声光互动扩展包</a></td></tr>
<tr>
<td width="25%;"><img src="images/Perception-Gizmos.png" width="150px;"></td>
<td width="25%;"><img src="images/Variety-Gizmos.png" width="150px;"></td>
<td width="25%;"><img src="images/Talkative-Pet.png" width="150px;"></td>
</tr>
<tr>
<td width="25%;"><a href="manual/感知小发明.rar" download style="font-weight:bold;">感知小发明扩展包</a></td>
<td width="25%;"><a href="manual/百变小发明.rar" download style="font-weight:bold;">百变小发明扩展包</a></td>
<td width="25%;"><a href="manual/萌宠声乐.rar" download style="font-weight:bold;">萌宠声乐扩展包</a></td></tr>
</table>
