# 运算类积木

## 1. （）+（）

执行加法运算。

![](images/operators-1-1.png)

**示例：**

![](images/operators-1-2.png)

按下空格键，mBot 的外接表情面板会显示2加3的计算结果。

---

## 2. （）-（）

执行减法运算。

![](images/operators-2-1.png)

**示例：**

![](images/operators-2-2.png)

按下空格键，mBot 的外接表情面板会显示3减1的计算结果。

---

## 3.（）*（）

执行乘法运算。

![](images/operators-3-1.png)

**示例：**

![](images/operators-3-2.png)

按下空格键，mBot 的外接表情面板会显示2乘以3的计算结果。

---

## 4. （）/（）

执行除法运算。

![](images/operators-4-1.png)

**示例：**

![](images/operators-4-2.png)

按下空格键，mBot 的外接表情面板会显示6除以2的计算结果。

---

## 5. 在（）和（）之间取随机数

在指定区间内取随机数。

![](images/operators-5-1.png)

**示例：**

![](images/operators-5-2.png)

按下空格键，mBot 的外接表情面板会显示指定图案，时间为1和10之间的随机数秒。

---

## 6. （）>（）

如果指定参数的值大于指定值，报告条件成立。

![](images/operators-6-1.png)

**示例：**

![](images/operators-6-2.png)

点击绿色旗帜运行程序，如果 mBot 的板载光线传感器检测到光线强度大于50，所有LED灯会亮起红色。

---

## 7. （）<（）

如果指定参数的值小于指定值，报告条件成立。

![](images/operators-7-1.png)

**示例：**

![](images/operators-7-2.png)

点击绿色旗帜运行程序，如果 mBot 的板载光线传感器检测到光线强度小于50，所有LED灯会亮起红色。

---

## 8. （）=（）

如果指定参数的值等于指定值，报告条件成立。

![](images/operators-8-1.png)

**示例：**

![](images/operators-8-2.png)

点击绿色旗帜运行程序，如果 mBot 接口3连接的超声波传感器检测到障碍物距离为50厘米，所有LED灯会亮起红色。

---

## 9. （）与（）

指定两个条件同时成立，报告条件成立。

![](images/operators-9-1.png)

**示例：**

![](images/operators-9-2.png)

点击绿色旗帜运行程序，如果 mBot 的板载按钮被松开并且红外遥控器的A按钮被按下，所有LED灯会亮起红色。

---

## 10. （）或（）

指定两个条件其中一个成立，报告条件成立。

![](images/operators-10-1.png)

**示例：**

![](images/operators-10-2.png)

点击绿色旗帜运行程序，如果 mBot 的板载按钮被按下或者红外遥控器的A按钮被按下，所有LED灯会亮起红色。

---

## 11. （）不成立

指定条件不成立，报告条件成立。

![](images/operators-11-1.png)

**示例：**

![](images/operators-11-2.png)

点击绿色旗帜运行程序，只要 mBot 的板载按钮不被按下，所有LED灯会亮起红色。

---

## 12. 连接（）和（）

报告两个字符串合并结果。

![](images/operators-12-1.png)

**示例：**

![](images/operators-12-2.png)

按下空格键，mBot 的外接表情面板会显示“hi”、“morning”。

---

## 13. （）的第（）个字符

报告指定字符串的指定位置字符。

![](images/operators-13-1.png)

**示例：**

![](images/operators-13-2.png)

按下空格键，mBot 的外接表情面板会显示“morning”的第3个字符“r”。

---

## 14. （）的字符数

报告指定字符串的字符数。

![](images/operators-14-1.png)

**示例：**

![](images/operators-14-2.png)

按下空格键，mBot 的外接表情面板会显示“morning”的字符数。

---

## 15. （）包含（）？

如果指定字符串包含另一指定字符串，报告条件成立。

![](images/operators-15-1.png)

**示例：**

![](images/operators-15-2.png)

点击绿色旗帜运行程序，如果“apple”包含“a”，mBot 所有的LED会亮起红色。

---

## 16. （）除以（）的余数

报告指定两数相除的余数。

![](images/operators-16-1.png)

**示例：**

![](images/operators-16-2.png)

按下空格键，mBot 的外接表情面板会显示9除以6的余数。

---

## 17. 四舍五入（）

报告指定数字四舍五入的值。

![](images/operators-17-1.png)

**示例：**

![](images/operators-17-2.png)

按下空格键，mBot 的外接表情面板会显示10.7四舍五入的结果。

---

## 18. （绝对值）（）

报告指定数字的指定数学运算结果，包括绝对值、向下取整、向上取整、平方根、sin、cos、tan、asin、acos、atan、ln、log、e^、10^，共15选项。

![](images/operators-18-1.png)

**示例：**

![](images/operators-18-2.png)

按下空格键，mBot 的外接表情面板会显示16的平方根。

---



