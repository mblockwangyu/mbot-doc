# 声光

**mBot 的 LED 灯**

<img src="../../en/block-reference/images/led.png" width="300px;" style="padding:5px 5px 15px 5px">

如上图所示，mBot的主板前方有两颗RGB LED灯。

## 1. 亮起（全部）灯，颜色为（），持续（）秒

使指定LED灯亮起指定颜色，并持续指定的一段时间。

![](images/show-1-1.png)

**示例：**

![](images/show-1-2.png)

按下空格键，mBot 全部的LED灯会亮起红色，并持续1秒。

---

## 2. 亮起（全部）灯，颜色为（）

使指定LED灯亮起指定颜色。

![](images/show-2-1.png)

**示例：**

![](images/show-2-2.png)

按下空格键，mBot 全部的LED灯会在红色和黄色间切换，并重复10次。

---

## 3. 亮起（全部）灯，颜色为红（）绿（）蓝（）

使指定LED灯亮起指定颜色，由红、绿、蓝混合而成。

![](images/show-3-1.png)

**示例：**

![](images/show-3-2.png)

按下空格键，mBot 全部的LED灯会亮起红色。

---

## 4. 播放音符（C4）持续（0.25）拍

播放指定音符，并持续指定拍数。

![](images/show-4-1.png)

**示例：**

![](images/show-4-2.png)

按下空格键，mBot 会播放音符C4，持续0.25拍的时间。

---

## 5. 播放声音以频率（700）赫兹，持续（1）秒

以指定赫兹播放声音，持续指定时间。

![](images/show-5-1.png)

**示例：**

![](images/show-5-2.png)

按下空格键，mBot 会播放频率为700赫兹的声音1秒钟。

---
