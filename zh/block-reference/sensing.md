# 感知

## 1. 光线传感器（板载）光线强度

报告指定光线传感器检测到的光线强度。

![](images/sensing-1-1.png)

**示例：**

![](images/sensing-1-2.png)

按下空格键，板载光线传感器检测到的光线强度会显示在 mBot 的外接表情面板。

---

## 2. 超声波传感器（接口3）距离 cm

报告指定超声波传感器检测到的障碍物距离（cm）。

![](images/sensing-2-1.png)

**示例：**

![](images/sensing-2-2.png)

按下空格键，接口3连接的超声波传感器检测到的障碍物距离会显示在 mBot 的外接表情面板。

---

## 3. 巡线传感器（接口2）读数

报告指定巡线传感器的读数。

![](images/sensing-3-1.png)

**示例：**

![](images/sensing-3-2.png)

按下空格键，接口2连接的巡线传感器读数会显示在 mBot 的外接表情面板。

---

## 4. 巡线传感器（接口2）检测到（左边）为（黑）？

如果指定巡线传感器在指定方向检测到的物体为指定颜色，报告条件成立。

![](images/sensing-4-1.png)

**示例：**

![](images/sensing-4-2.png)

点击绿色旗帜运行程序，如果 mBot 接口2连接的巡线传感器检测到左边的障碍物为黑色，mBot 就会停止运动。

---

## 5. 板载按钮（已按下）？

如果 mBot 的板载按钮以被按下，报告条件成立。

![](images/sensing-5-1.png)

**示例：**

![](images/sensing-5-2.png)

点击绿色旗帜运行程序，如果 mBot 的板载按钮被按下，所有LED灯会亮起红色。

---


## 6. 红外遥控器按下（A）？

如果红外遥控器的指定按钮被按下，报告条件成立。

![](images/sensing-6-1.png)

**示例：**

![](images/sensing-6-2.png)

点击绿色旗帜运行程序，如果红外遥控器的按钮A被按下，mBot 的外接表情面板会显示“yes”。

---


## 7. 发送红外消息（）

发送指定红外消息。

![](images/sensing-7-1.png)

**示例：**

![](images/sensing-7-2.png)

按下空格键，mBot 会发送红外消息“hello”。

---

## 8. 接收到的红外消息

报告接收到的红外消息。

![](images/sensing-8-1.png)

**示例：**

![](images/sensing-8-2.png)

按下空格键，mBot 接收到的红外消息会显示在外接表情面板上。

---


## 9. 计时器

报告计时器的值。

![](images/sensing-9-1.png)

**示例：**

![](images/sensing-9-2.png)

按下空格键，mBot 计时器的值会显示在外接表情面板上。

---


## 10. 计时器归零

将 mBot 的计时器归零。

![](images/sensing-10-1.png)

**示例：**

![](images/sensing-10-2.png)

按下空格键，mBot 的计时器会被重置。

---
