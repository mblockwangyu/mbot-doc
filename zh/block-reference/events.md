# 事件

## 1. 当绿色旗帜被点击

当绿色旗帜被点击，运行其下程序。

![](images/events-1-1.png)

**示例：**

![](images/events-1-2.png)

点击绿色旗帜，mBot 的外接表情面板会显示指定图案。

---

## 2. 当按下（空格）键

当键盘指定按键被按下，运行其下程序。

![](images/events-2-1.png)

**示例：**

![](images/events-2-2.png)

按下空格键，mBot 的外接表情面板会显示指定图案。

---

## 3. 当板载按钮（已按下）

当板载按钮已被按下或松开，运行其下程序。

![](images/events-3-1.png)

**示例：**

![](images/events-3-2.png)

当板载按钮被按下，mBot 所有的LED灯会亮起红色。

---

## 4. 当接收到（消息1）

当接收到指定消息，运行其下程序。

![](images/events-4-1.png)

**示例：**

![](images/events-4-2.png)

当 mBot 接收到“message1”，会以50%动力前进1秒。

---

## 5. 广播（消息1）

广播指定消息。

![](images/events-5-1.png)

**示例：**

![](images/events-5-2.png)

按下空格键，mBot 会广播“message1”。

---

## 6. 广播（消息1）并等待

广播指定消息，并等待被该条广播启动的程序执行完毕。

![](images/events-6-1.png)

**示例：**

![](images/events-6-2.png)

按下空格键，mBot 会广播“message1”，并等待所有被“message1”启动的程序执行完毕。

---
