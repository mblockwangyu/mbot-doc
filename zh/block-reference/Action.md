# 运动

## 1. 前进以动力（50）%，持续（1）秒

让 mBot 以指定动力前进指定时间。

![](images/action-1-1.png)

**示例：**

![](images/action-1-2.png)

按下“&uarr;”键，mBot 会以50%的动力前进1秒。

---

## 2. 后退以动力（50）%，持续（1）秒

让 mBot 以指定动力后退指定时间。

![](images/action-2-1.png)

**示例：**

![](images/action-2-2.png)

按下“&darr;”键，mBot 会以50%的动力后退1秒。

---

## 3. 左转以动力（50）%，持续（1）秒

让 mBot 以指定动力左转指定时间。

![](images/action-3-1.png)

**示例：**

![](images/action-3-2.png)

按下“&larr;”键，mBot 会以50%的动力左转1秒。

---

## 4. 右转以动力（50）%，持续（1）秒

让 mBot 以指定动力右转指定时间。

![](images/action-4-1.png)

**示例：**

![](images/action-4-2.png)

按下“&rarr;”键，mBot 会以50%的动力右转1秒。

---

## 5. （前进）以动力（50）%

让 mBot 以指定动力向指定方向运动，有四个选项：前进、后退、左转、右转。

![](images/action-5-1.png)

**示例：**

![](images/action-5-2.png)

按下空格键，mBot 会持续以50%动力前进。

---

## 6. 左轮转动以动力（50）%，右轮转动以动力（50）%

分别以指定动力转动 mBot 的左轮和右轮。

![](images/action-6-1.png)

**示例：**

![](images/action-6-2.png)

按下空格键，mBot 的左轮和右轮都会以50%的动力转动。

---

## 7. 停止运动

让 mBot 停止运动。

![](images/action-7-1.png)

**示例：**

![](images/action-7-2.png)

按下空格键，mBot 会停止运动。

---
