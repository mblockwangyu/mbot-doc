# 显示

<img src="../../en/block-reference/images/led-panel.jpg" width="300px;" style="padding: 5px 5px 15px 5px;">

使用表情面板增加 mBot 的可玩性。

表情面板可通过 RJ25 线连接到主板上对应的接口。

**坐标**

<img src="../../en/block-reference/images/led-matrix.png" width="400px;" style="padding: 5px 5px 15px 5px;">

如上图所示，表情面板以左上角为坐标 0 点， x ，y 的方向如箭头示意。参数有效范围：
- x: -15 ~ 15
- y: -7 ~ 7

## 1. 表情面板（接口1）显示图案（）持续（）秒

在指定接口连接的表情面板显示指定图案，持续一段时间后熄灭。

![](images/looks-1-1.png)

**示例：**

![](images/looks-1-2.png)

按下空格键，mBot 的外接表情面板会显示指定图案1秒。

---

## 2. 表情面板（接口1）显示图案（）

在指定接口连接的表情面板显示指定图案。

![](images/looks-2-1.png)

**示例：**

![](images/looks-2-2.png)

按下空格键，mBot 的外接表情面板会在指定的两个图案间切换显示，重复10次，实现眨眼效果。

---

## 3. 表情面板（接口1）显示图案（） x：（）y：（）

在指定接口连接的表情面板的指定位置显示指定图案。

![](images/looks-3-1.png)

**示例：**

![](images/looks-3-2.png)

按下空格键，mBot 的外接表情面板会在（0，0）位置显示指定图案。

---

## 4. 表情面板（接口1）显示字符（）

在指定接口连接的表情面板显示指定字符。

![](images/looks-4-1.png)

**示例：**

![](images/looks-4-2.png)

按下空格键，mBot 的外接表情面板会显示“hello”。

---

## 5. 表情面板（接口1）显示字符（）在x：（）y：（）

在指定接口连接的表情面板的指定位置显示指定字符。

![](images/looks-5-1.png)

**示例：**

![](images/looks-5-2.png)

按下空格键，mBot 的外接表情面板会在（0，0）显示“hello”。

---

## 6. 表情面板（接口1）显示数字（）

在指定接口连接的表情面板显示指定数字。

![](images/looks-6-1.png)

**示例：**

![](images/looks-6-2.png)

按下空格键，mBot 的外接表情面板会显示“2019”。

---

## 7.  表情面板（接口1）显示时间（）：（）

在指定接口连接的表情面板显示指定时间。

![](images/looks-7-1.png)

**示例：**

![](images/looks-7-2.png)

按下空格键，mBot 的外接表情面板会显示时间“15：30”。

---

## 8. 表情面板（接口1）熄灭面板

熄灭指定接口连接的表情面板。

![](images/looks-8-1.png)

**示例：**

![](images/looks-8-2.png)

按下空格键，mBot 的外接表情面板会熄灭。

---