# 人工智能机器人教育套装
<img src="images/parts-list.jpg" width="400px;" style="padding: 5px 5px 15px 0px;">

人工智能机器人教育套装包含一台完整的 mBot 机器人，十余种电子模块及其配件。其中的视觉模块是一个具备本地学习、计算、识别物体能力的摄像头。此套件旨在帮助学生学习并应用电子模块完成相关的人工智能项目，从而了解、体验和实践人工智能。

## 了解 mBot 和视觉模块

有关 mBot 的简介、搭建及快速上手指南，参见 [mBot 帮助文档](http://docs.makeblock.com/mbot/zh).

有关视觉模块的使用和功能介绍，参见[视觉模块](http://docs.makeblock.com/diy-platform/zh/mbuild/hardware/sensors/smart-camera.html).

## 获取在线课程

我们为此套件提供了配套的在线课程，可登陆[慧课程](https://edu.makeblock.com/)免费获取。

如你已有慧编程账号，可直接使用该账号登陆，然后通过以下步骤获取课程：

1、选择“课程资源” > “官方课程”，然后点击“课程超市”。

<img src="images/lessons.png" width="600px;" style="padding: 5px 5px 15px 0px;">

2、在搜索框中输入“人工智能机器人”并点击“搜索”。

<img src="images/lessons-1.png" width="600px;" style="padding: 5px 5px 15px 0px;">

3、点击“免费获取”。

<img src="images/lessons-2.png" width="200px;" style="padding: 5px 5px 15px 0px;">

## 机器人搭建

<p><span style="background-color:#2eb3e8;line-height:30px;padding:10px;font-size:14px;border-radius:3px;"><a href="images/人工智能机器人搭建手册.rar" download style="color:white;font-weight:bold;">搭建手册下载</a></span></p>

通过此套件提供的各种电子模块、结构件及其配件，你可以搭建多种不同形态的机器人。此处仅提供课程中涉及的形态搭建，包括如下：

<img src="images/buildings.png" width="400px;" style="padding: 5px 5px 15px 0px;">






