# mBotシリーズ製品のmBlock接続について

<table style="border:1px solid black;width:100%;background-color:#E6E6FA;" cellpadding=6px;>
<tr style="border:1px solid black;background-color:#708090;">
<th colspan="3">異なる接続方法によるmBotシリーズのmBlock接続可否一覧</th>
</tr>

<tr style="border:1px solid black;">
<td>接続方法</td> <td>mBlock 3</td> <td>mBlock 5 (v5.0.0-RC3及び以上）</td>
</tr>

<tr style="border:1px solid black;">
<td>PC内蔵Bluetooth </td> <td>不可</td> <td>Bluetooth 4.0経由のみ可※</td>
</tr>

<tr style="border:1px solid black;">
<td>USBケーブル</td> <td>可</td> <td>可</td>
</tr>

<tr style="border:1px solid black;">
<td>Makeblock Bluetoothドングル（別売り）</td> <td>可</td> <td>可</td>
</tr>

<tr style="border:1px solid black;">
<td>2.4G モジュール (別売り)</td> <td>可</td> <td>不可</td>
</tr>

</table>

<br>
**※詳細は下記内容[「方法2：Bluetooth 4.0経由で接続する」](#method2)をご参照ください**

## ハードウェアデバイスの接続

mBlock 5は、Makeblock製の製品だけでなく、他社製のデバイスも幅広くサポートしています。初心者向けのガイドに従って、多様なデバイスを接続することが可能です。

### mBlock 5を使用してデバイスを接続する

mBlock 5には、USB経由とBluetooth 4.0経由の2つの方法でデバイスを接続することができます。

**注：mBlock 5では、一度に接続できるデバイスは1台のみです。新しいデバイスを接続すると、既存の接続が切断されますのでご注意ください。**

#### 方法1：USB経由で接続する

USBケーブルを介してコンピュータに接続することができます（または、Makeblock Bluetoothドングルを使用してコンピュータに接続することも可能です）。

1\. 購入した製品のパッケージ内にあるUSBケーブルを使用して、デバイスとパソコンのUSBポートを接続します。

2\. デバイスがコンピュータと接続したことを確認します。

3\. デバイスの電源を入れます。

4\. mBlock 5上で接続されているデバイスを選択し、「接続」をクリックします。以下の画像をご参照ください。<br>
![](images/connect-1.png)

5\. 「デバイス接続」のポップアップが表示されますので、「USB」を選択してください。デバイスのシリアルポートが自動的に検出されるので、「接続」をクリックします。以下の画面が表示されます。<br>
![](images/connect-2.png)

**注：**
- **複数のデバイスがコンピュータに接続されている場合は、接続したいデバイスのシリアルポートをプルダウンメニューから選択してください。**
- **接続可能なすべてのデバイスを表示したい場合は、「すべての接続可能デバイスを表示」のチェックボックスをオンにします。**

6\. 接続が完了したら、デバイスがmBlock 5に正常に接続されていることを確認してください。正常に接続されていない場合は、デバイスとの接続を切断して再接続してください。

**Makeblock Bluetoothドングル（別売り）**

Makeblock Bluetoothドングルは、Makeblock製品専用の接続アダプタで、USBケーブルを使用せずに無線接続できるよう設計されています。専用の説明ガイドを参照してご利用ください。

#### 方法2：Bluetooth 4.0経由で接続する{#method2}

**注1：Bluetooth 4.0接続はPCシステムの環境制限やプログラミングのアップロードなどの機能に対応していないため、USB経由による接続とMakeblock専用ドングル経由での接続をお勧めしています。**

**注2：Bluetooth 4.0経由での接続は、MakeblockのmBotシリーズ製品のみ対応しています。**

Bluetooth 4.0経由で、コンピュータにデバイスを無線接続することができます。システム環境の要件は次の通りです。<br>
- **Windows**：PCに内蔵のBluetoothバージョンは、Bluetooth 4.0が必須となります。他のBluetoothバージョンの場合は、Bluetooth 4.0のアダプタをご購入ください（詳細については、「市販のBluetooth 4.0アダプタを使用する」をご覧ください）。
- **Mac OS**：主要な機種の最新版OSをサポートしています。

1\. デバイスの電源を入れます。Bluetoothモジュールの青色のLEDライトが点滅します。

2\. コンピュータのBluetoothをオンにします
- **Windows**：タスクバーの「アクションセンター」を選択し、「Bluetooth」を選択
- **Mac OS**：「Appleメニュー」から「システム環境」を選択し、「Bluetooth」を選択

3\. ｍBlock 5で接続したいデバイスを選択し、「接続」をクリックします。<br>
![](images/connect-3.png)

4\.「デバイス接続」のポップアップが表示されますので、「Bluetooth 4.0」を選択してください。デバイスが自動的に検出されるので、「接続」をクリックします。以下の画面が表示されます。<br>
![](images/connect-4.png)

**注：**
- **パソコンで同時に複数のデバイスが検出された場合は、プルダウンメニューから接続するデバイスを選択します。**
- **接続可能なすべてのデバイスを表示する必要がある場合は、「すべての接続可能なデバイスを表示」のチェックボックスをオンにします。**

デバイスが自動的に検出されない場合は、下記のようにBluetooth 4.0ドライブをインストールして、再試行してください。

### Bluetooth 4.0ドライブのインストール方法

**注：Bluetooth 4.0ドライブをインストールすると、元々コンピュータに搭載されているBluetoothが無効になり、PCのデバイスマネージャーの中のBluetoothロゴが削除される可能性があります。**

上記のような状況が発生した場合は、ページ最後にある[「Bluetooth 4.0ドライブをアンインストールする」](#uninstall)説明手順に従って、元の状態に戻すことができます。

1\. [Zadig](https://zadig.akeo.ie/)ツールをダウンロードします

2\. Zadigを起動し、「Options」を選択して、「List All Devices」を選択します。<br>
![](images/connect-5.png)

3\. プルダウンメニューのBluetoothデバイスを選択してから、「Replace Driver」をクリックします。<br>
![](images/connect-6.png)

### 市販のBluetooth 4.0アダプタを使用する

Windowsに内蔵されているBluetoothバージョンがBluetooth 4.0以外の場合は、市販のBluetooth 4.0アダプタを使用して、デバイスをmBlock 5に接続することが可能です。接続手順は「方法2：Bluetooth 4.0経由で接続する」をご参照ください。

#### mBot Bluetoothモジュールの接続手順

1\. mBotを起動して、Bluetoothモジュールが正しく取り付けられていることを確認します(モジュールの青色のLEDライトがゆっくりと点滅します)。

2\. USB経由でmBotとコンピュータを接続してください。上記の「方法1：USB経由で接続する」を参照して、ファームウェアをアップデートしてください。

3\. コンピュータのハードウェアおよびシステムの制限については、上記の「方法2：Bluetooth 4.0経由で接続する」を参照してください。モジュールの青色のLEDライトが常に点灯している場合は、接続が成功したことになります。

4\. mBlock 3は、現在Bluetoothモジュールの直接接続をサポートしていませんので、Bluetoothドングルに合わせて使用することをお勧めします。

##### 「Bluetooth 4.0ドライブをアンインストール」{#uninstall}

1\. キーボードのWindows+Xでスタートメニューを開き、「デバイスマネージャー」を選択します。<br>
![](images/connect-7.png)

2\. 「ユニバーサル シリアル パス デバイス」をクリックすると、インストールされたBluetoothドライブが表示されます。右クリックして「デバイスのアンインストール」をクリックします。<br>
<img src="images/connect-8.png" style="width:480px;">

3\. 「デバイスのアンインストール」のポップアップが表示されます。「このデバイスのドライバー ソフトウェアを削除します」のチェックボックスをオンにして、アンインストールをクリックします。<br>
<img src="images/connect-9.png" style="width:400px;">

4\. アンインストールが完了したら、パソコンを再起動してください。

5\. 元のBluetooth状態に回復します。<br>
![](images/connect-10.png)

### WindowsのBluetoothバージョンを確認する

1\. キーボードのWindows+Xでスタートメニューを開き、「デバイスマネージャー」を選択します。<br>
![](images/connect-11.png)

2\. <span style="text-align:middle;"><img src="images/bluetooth-icon.jpg" style="width:15px;"></span>「Bluetooth」では、複数のBluetoothデバイスが表示されます。

3\. 該当するBluetooth名を選択し、「Bluetooth」デバイスのドライバの「プロパティ」を右クリックで選択してください。<br>
<img src="images/connect-12.png" style="width:480px;">

4\. 「詳細設定」をクリックしてファームウェアのバージョンを表示します。LMP番号には、コンピュータが使用しているBluetoothバージョンが表示されます。<br>
<img src="images/connect-13.png" style="width:400px;">

5\. 以下の表から、LMP番号と対応するBluetoothバージョンを照合します。<br>

<table style="border:1px solid black;width:480px;background-color:#E6E6FA;">
<tr style="border:1px solid black;text-align:left;">
<th>LMP番号</th> <th>Bluetoothバージョンを照合します</th>
</tr>

<tr style="border:1px solid black;">
<td>LMP 9.x</td> <td>Bluetooth 5.0</td>
</tr>

<tr style="border:1px solid black;">
<td>LMP 8.x</td> <td>Bluetooth 4.2</td>
</tr>

<tr style="border:1px solid black;">
<td>LMP 7.x</td> <td>Bluetooth 4.1</td>
</tr>

<tr style="border:1px solid black;">
<td>LMP 6.x</td> <td>Bluetooth 4.0</td>
</tr>

<tr style="border:1px solid black;">
<td>LMP 5.x</td> <td>Bluetooth 3.0 + HS</td>
</tr>

<tr style="border:1px solid black;">
<td>LMP 4.x</td> <td>Bluetooth 2.1 + EDR</td>
</tr>

<tr style="border:1px solid black;">
<td>LMP 3.x</td> <td>Bluetooth 2.0 + EDR</td>
</tr>

<tr style="border:1px solid black;">
<td>LMP 2.x</td> <td>Bluetooth 1.2</td>
</tr>

<tr style="border:1px solid black;">
<td>LMP 1.x</td> <td>Bluetooth 1.1</td>
</tr>

<tr style="border:1px solid black;">
<td>LMP 0.x</td> <td>Bluetooth 1.0b</td></tr>
</table>

<br>

## 製品の使用ガイドに基づいてデバイスを接続する

他社で製造されているデバイスは、各製品の説明ガイドに基づいてmBlock 5に接続する必要があります。例えば、BBCのmicro:bitなどを接続する際には、対応する製品の説明ガイドを参照してください。

