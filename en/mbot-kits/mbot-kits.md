# AI & IoT Robot Education Kit
<img src="images/parts-list.jpg" width="400px;" style="padding: 5px 5px 15px 0px;">

The AI & IoT Robot education kit includes an mBot robot and more than 10 electronic modules and accessories. The Smart Camera module included is a camera that can learn, calculate and detect objects. This kit is designed to help students understand and experience AI through learning and using electronic modules to create AI projects.

## About mBot and Smart Camera

For the introduction, building, and quick start guide of mBot, see [mBot Help](http://docs.makeblock.com/mbot/en).

For the instructions and function description of Smart Camera, see [Smart Camera](http://docs.makeblock.com/diy-platform/en/mbuild/hardware/sensors/smart-camera.html).

## Learning resources

 You can use this kit to teach or learn AI applications. Download the book you need:

<p><span style="background-color:#2eb3e8;line-height:30px;padding:10px;font-size:14px;border-radius:3px;"><a href="images/Teachers' Book.rar" download style="color:white;font-weight:bold;">Teachers' Book</a></span></p>

<p><span style="background-color:#2eb3e8;line-height:30px;padding:10px;font-size:14px;border-radius:3px;"><a href="images/Students' Book.rar" download style="color:white;font-weight:bold;">Students' Book</a></span></p>

## Robot building instructions

<p><span style="background-color:#2eb3e8;line-height:30px;padding:10px;font-size:14px;border-radius:3px;"><a href="images/Building Instructions.rar" download style="color:white;font-weight:bold;">Building Instructions</a></span></p>

With the electronic modules, structural parts, and accessories thereof, you can build multiple robots. The building instructions provided here cover only those used in the Teachers' and Students' books, as shown in the following figure.

<img src="images/buildings-en.png" width="400px;" style="padding: 5px 5px 15px 0px;">




