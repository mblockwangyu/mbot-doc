# Expand mBot
You can add a variety of extensions and components to mBot. With these extensions, mBot can be transformed into a dancing cat, a crazy frog and so on. 
## Download the manuals of mBot add-on packs
<table  cellpadding="15px;" cellspacing="12px;" style="font-size: 12px;text-align:center;">
<tr>
<td width="25%;"><img src="images/six-legged.png" width="150px;"></td>
<td width="25%;"><img src="images/servo-pack.png" width="150px;"></td>
<td width="25%;"><img src="images/interactive-light-sound.png" width="150px;"></td>
</tr>
<tr>
<td width="25%;"><a href="manual/Six-legged Robot.rar" download style="font-weight:bold;">Six-legged Robot Add-on Pack</a></td>
<td width="25%;"><a href="manual/Servo Pack.rar" download style="font-weight:bold;">Servo Pack Add-on Pack</a></td>
<td width="25%;"><a href="manual/Interactive Light & Sound.rar" download style="font-weight:bold;">Interactive Light & Sound Add-on Pack</a></td></tr>
<tr>
<td width="25%;"><img src="images/Perception-Gizmos.png" width="150px;"></td>
<td width="25%;"><img src="images/Variety-Gizmos.png" width="150px;"></td>
<td width="25%;"><img src="images/Talkative-Pet.png" width="150px;"></td>
</tr>
<tr>
<td width="25%;"><a href="manual/Perception Gizmos.rar" download style="font-weight:bold;">Perception Gizmos Add-on Pack</a></td>
<td width="25%;"><a href="manual/Variety Gizmos.rar" download style="font-weight:bold;">Variety Gizmos Add-on Pack</a></td>
<td width="25%;"><a href="manual/Talkative Pet.rar" download style="font-weight:bold;">Talkative Pet Add-on Pack</a></td></tr>
</table>