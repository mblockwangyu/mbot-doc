# Connect mBot

You can connect mBot to mBlock 5 through a USB cable, Bluetooth 4.0, or the 2.4G module.
- [Method 1: using a USB cable](#method-1-using-a-usb-cable)
- [Method 2: using Bluetooth 4.0](#method-2-using-bluetooth-40)
- [Method 3: using the 2.4G module](#method-3-using-the-24g-module)

## Method 1: using a USB cable

1\. Use the USB Cable that came with mBot to connect your mBot to a USB port on you computer.

2\. Power on your mBot.

<img src="images/connect-mbot-1.png" width="300px;" style="padding:5px 5px 20px 5px;">

3\. Under **Devices**, click **add**, choose **mBot** from the pop-up **Device Library** window, and click **OK**.

<img src="images/connect-mbot-2.png" width="300px;" style="padding:5px;"><span style="font-size:30px;color:lightblue;padding:10px;">&rarr;</span> <img src="images/connect-mbot-3.png" style="padding:5px;">

4\. Click **Connect**, and from the pop-up device connection window, click **Connect** on the **USB** tab.

<img src="images/connect-mbot-4.png" width="300px;" style="padding:5px;"><span style="font-size:30px;color:lightblue;padding:10px;">&rarr;</span> <img src="images/connect-mbot-5.png" width="250px;" style="padding:5px;">

## Method 2: using Bluetooth 4.0

System requirements:
- **Windows**: the version of Bluetooth must be 4.0; for other Bluetooth versions, a Bluetooth 4.0 adapter is recommended (refer to [Bluetooth 4.0 Adapter](http://www.mblock.cc/doc/en/part-one-basics/connect-devices.html#3-bluetooth-40-instructions-for-windows-users) for detailed instructions)
- **Mac OS**: support most Mac OS models

1\. Power on your mBot.

<img src="images/connect-mbot-1.png" width="300px;" style="padding:5px 5px 20px 5px;">

2\. Turn on the Bluetooth on your PC.
- Windows: on the task bar, select **action center** > **Bluetooth**
- Mac OS: choose **Apple menu** > **System Preferences**, then click **Bluetooth**

3\. Under **Devices**, click **add**, choose **mBot** from the pop-up **Device Library** window, and click **OK**.

<img src="images/connect-mbot-2.png" width="300px;" style="padding:5px;"><span style="font-size:30px;color:lightblue;padding:10px;">&rarr;</span> <img src="images/connect-mbot-3.png" style="padding:5px;">

4\. Click **Connect**, and from the pop-up device connection window, click **Connect** on the **Bluetooth 4.0** tab.

<img src="images/connect-mbot-4.png" width="300px;" style="padding:5px;"><span style="font-size:30px;color:lightblue;padding:10px;">&rarr;</span> <img src="images/connect-mbot-6.png" width="250px;" style="padding:5px;">

## Method 3: using the 2.4G module

Computers with the following systems are recommended:
- **Windows**: Windows 7 or later
- **Mac OS**: macOS Sierra 10.12 or later

To connect mBot in this way, you need to use the 2.4G module and adapter.

<img src="images/connect-mbot-7.png" width="300px;" style="padding:5px;"><span style="font-size:30px;color:lightblue;padding:10px;">

You can use them simply by plugging them. No drivers or pairing is required.


1\. Insert the  2.4G module into mCore on mBot and insert the adapter into a USB port of a computer.


2\. Power on your mBot.

<img src="images/connect-mbot-1.png" width="300px;" style="padding:5px 5px 20px 5px;">

3\. Under **Devices**, click **add**, choose **mBot** from the pop-up **Device Library** window, and click **OK**.

<img src="images/connect-mbot-2.png" width="300px;" style="padding:5px;"><span style="font-size:30px;color:lightblue;padding:10px;">&rarr;</span><img src="images/connect-mbot-3.png" style="padding:5px;">

4\. Click **Connect**, and from the pop-up device connection window, click **Connect** on the **2.4G** tab.

<img src="images/connect-mbot-4.png" width="300px;" style="padding:5px;"><span style="font-size:30px;color:lightblue;padding:10px;">&rarr;</span> <img src="images/connect-mbot-8.png" width="250px;" style="padding:5px;">

After the successful connection, mBlock 5 displays the following information indicating the 2.4G connection.

<img src="images/connect-mbot-9.png" width="300px;" style="padding:5px;">

<div style="background-color:#EAF6FD;padding:10px;border-left:5px solid #BAE3F9;font-size: 14px;line-height:24px;margin-bottom:20px;border-radius:5px;"><strong>Note:</strong><br>The firmware of mBot cannot be updated through 2.4G connection.<br>If mBlock 5 prompts you to update the firmware of mBot during programming, exit from the the 2.4G connection and connect mBot to mBlock 5 in the USB mode to update its firmware. Then you can connect mBot to mBlock 5 in the 2.4G mode for programming.
</div>