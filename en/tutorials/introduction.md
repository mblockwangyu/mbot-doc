<img src="../../zh/faq/images/mbot.jpg" width="200px;" style="padding:10px 10px 20px;">

# Introduction

As Makeblock's award-winning programming robot, mBot has taken the heart of over 4,500,000 children around the world. Not only do children adore mBot's loving appearance, but they also love playing with it. 

With a screwdriver and step-by-step instructions, children can build their own robot from scratch and enjoy the fun of hands-on creation. The building process provides a perfect opportunity to introduce kids to the basics of robotic machinery and electronic parts. They can easily get started with block-based programming to play with mBot.

## Use mBlock 5

Use mBlock 5 to play with your mBot. Graphical programming is ideal to show kids the magic of programming. As they progress, they can even further delve into more complicated Arduino C programming.

<img src="images/mblock5.png" width="400px;" style="padding: 5px 5px 15px 0px;">

You can get mBlock 5 on your PC, phones and tablets, or use mBlock 5 in a web browser.

- For PC, please visit: [http://www.mblock.cc/mblock-software/](http://www.mblock.cc/mblock-software/)
- For Android and iOS, please search "mblock" in any application store to download
- For web browser, please visit: [https://ide.makeblock.com/](https://ide.makeblock.com/)

**_Note:_** _In this document, screenshots are generated from mBlock PC and are for references only. But all projects are available both on PC and mobile devices._