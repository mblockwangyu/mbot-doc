# Preset Modes

There are three preset modes of mBot:
- [Obstacle avoidance](#obstacle-avoidance-mode)
- [Infrared remote control](#infrared-remote-control-mode)
- [line-following](#line-following-mode)

Switch modes using the <span style="color:lightblue;"><b>infrared remote controller or the on-board button</b></span>.

<img src="images/modes.png" style="padding:5px 5px 15px 0px;">

<div margin="10px;" style="background-color:#E4F3F7;color:dark-grey;padding:20px;" width="60%;">
The color of the LED on the main board indicates the current mode:<br>
<span style="padding:2px;"><img src="images/led-1.png" width="20px"></span> White: infrared remote control mode<br>
<span style="padding:2px;"><img src="images/led-2.png" width="20px"></span> Green: obstacle avoidace mode<br>
<span style="padding:2px;"><img src="images/led-3.png" width="20px"></span> Blue: line-following mode<br>
</div>

<br>

## Obstacle avoidance mode

In obstacle avoidance mode, mBot moves and avoids obstacles autonomously.

<img src="images/modes-2.png" style="padding:5px 5px 15px 5px;">

## Infrared remote control mode

A special infrared remote controller is included in the package. You can use the controller to control mBot, such as speed, direction, and others. 

**Note: place your mBot on a piece of smooth and flat ground.**

<img src="images/modes-3.png" style="padding:5px 5px 15px 5px;">

## Line-following mode

In line-following mode, mBot moves autonomously along the black lines on the map.

<img src="images/modes-4.png" style="padding:5px 5px 15px 5px;">