# Building mBot

- [Parts list](#parts-list)
- [Use of screwdriver](#use-of-screwdriver)
- [Main board](#main-board)
- [Building instructions](#building-instructions)

## Parts list

<img src="images/parts-list.png" width="800px;" style="padding: 5px 5px 15px 0px;">

## Use of screwdriver

<img src="images/screw-driver.png" style="padding: 5px 5px 15px 0px;">

## Main board

<img src="images/main-board.png" style="padding: 5px 5px 15px 0px;">

## Building instructions

<img src="images/building-1.png" style="padding: 5px 5px 15px 0px;">

<img src="images/building-2.png" style="padding: 5px 5px 15px 0px;">

<img src="images/building-3.png" style="padding: 5px 5px 15px 0px;">

<img src="images/building-4.png" style="padding: 5px 5px 15px 0px;">

<img src="images/building-5.png" style="padding: 5px 5px 15px 0px;">

<img src="images/building-6.png" style="padding: 5px 5px 15px 0px;">

<img src="images/building-7.png" style="padding: 5px 5px 15px 0px;">