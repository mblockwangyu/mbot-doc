# Get Started

Let's get started with a simple project. The LEDs of mBot will switch between red and yellow.

<img src="images/get-started-5.gif" width="250px;" style="padding:5px 5px 20px 5px;">

### Connect mBot

1\. Click "Connect" to connect mBot.

<img src="images/get-started-1.gif" width="800px;" style="padding:5px 5px 20px 5px;">

### Start programming

2\. Drag two Show blocks <span style="background-color:#9013FE;color:white;padding:3px; font-size: 12px">LED(all) shows color () for () secs</span> to the Scripts area. Change the color of the second block to yellow.

<img src="images/get-started-2.gif" width="800px;" style="padding:5px 5px 20px 5px;">

3\. Add a Control block <span style="background-color:#FFAB19;color:white;padding:3px; font-size: 12px">repeat (10)</span> to wrap up the two Show blocks.

<img src="images/get-started-3.gif" width="800px;" style="padding:5px 5px 20px 5px;">

4\. Add an Events block <span style="background-color:#FFBF00;color:white;padding:3px; font-size: 12px">when green flag clicked</span> and snap it on top of other blocks.

<img src="images/get-started-4.gif" width="800px;" style="padding:5px 5px 20px 5px;">

5\. Click the green flag under the stag and see what happens.

<img src="images/get-started-6.png" width="300px;" style="padding:5px 5px 20px 5px;">