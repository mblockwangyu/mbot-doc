# Control

## 1. wait () seconds

Waits for a specified period of time to run the script.

![](images/control-1-1.png)

**Example:**

![](images/control-1-2.png)

When the space key is pressed, all LEDs will light up red, and 1 second later, switch to yellow.

---

## 2. repeat ()

Runs the script for the specified number of times.

![](images/control-2-1.png)

**Example:**

![](images/control-2-2.png)

When the space key is pressed, all LEDs will switch between red and yellow for 10 times.

---

## 3. forever

Runs the script repeatedly.

![](images/control-3-1.png)

**Example:**

![](images/control-3-2.png)

When the space key is pressed, all LEDs will switch between red and yellow.

---

## 4. if () then ()

If the report condition is met, run the script.

![](images/control-4-1.png)

**Example:**

![](images/control-4-2.png)

When the green flag is clicked, if the on-board button is pressed, all LEDs will light up red.

---

## 5. if () then () else ()

If the report condition is met, run script 1. If not, run script 2.

![](images/control-5-1.png)

**Example:**

![](images/control-5-2.png)

When the space key is pressed, if the on-board button is pressed, all LEDs will light up red, else, green.

---

## 6. wait ()

Wait until the report condition is met. Then run the script.

![](images/control-6-1.png)

**Example:**

![](images/control-6-2.png)

When the green flag is clicked, if the on-board button is pressed, all LEDs will light up red.

---

## 7. repeat until ()

Run the script repeatedly until the report condition is met.

![](images/control-7-1.png)

**Example:**

![](images/control-7-2.png)

When the green flag is clicked, all LEDs will light up red until the on-board button is pressed.

---

## 8. stop ()

Stop the specified script or scripts. There are three options: all, this script, or other scripts in sprite.

![](images/control-8-1.png)

**Example:**

![](images/control-8-2.png)

When the space key is pressed, all scripts will stop.

---