# Operators

## 1. () + ()

Performs mathematical addition.

![](images/operators-1-1.png)

**Example:**

![](images/operators-1-2.png)

When the space key is pressed, the external LED panel will display the result of "2 + 3".

---

## 2. () &minus; ()

Performs mathematical subtraction.

![](images/operators-2-1.png)

**Example:**

![](images/operators-2-2.png)

When the space key is pressed, the external LED panel will display the result of "3 - 1".

---

## 3. () * ()

Performs mathematical multiplication.

![](images/operators-3-1.png)

**Example:**

![](images/operators-3-2.png)

When the space key is pressed, the external LED panel will display the result of "2 &times; 3".

---

## 4. () / ()

Performs mathematical division.

![](images/operators-4-1.png)

**Example:**

![](images/operators-4-2.png)

When the space key is pressed, the external LED panel will display the result of "6 &divide; 2".

---

## 5. pick random () to ()

Picks a random number from the specified range.

![](images/operators-5-1.png)

**Example:**

![](images/operators-5-2.png)

When the space key is pressed, the external LED panel will display the image for a random number of seconds within the range of 1 to 10.

---

## 6. () &gt; ()

If the value of the specified parameter is greater than the specified value, the report condition is met.

![](images/operators-6-1.png)

**Example:**

![](images/operators-6-2.png)

When the green flag is clicked, if the light intensity is greater than 50, all LEDs will light up red.

---

## 7. () &lt; ()

If the value of the specified parameter is less than the specified value, the report condition is met.

![](images/operators-7-1.png)

**Example:**

![](images/operators-7-2.png)

When the green flag is clicked, if the light intensity is smaller than 50, all LEDs will light up red.

---

## 8. () = ()

If the value of the specified parameter equals the specified value, the report condition is met. 

![](images/operators-8-1.png)

**Example:**

![](images/operators-8-2.png)

When the greed flag is clicked, if the distance of obstacle detected by the ultrasonic sensor equals 50 cm, all LEDs will light up red.

---

## 9. () and ()

If both the conditions are met, the report condition is met.

![](images/operators-9-1.png)

**Example:**

![](images/operators-9-2.png)

When the greed flag is clicked, if the on-board button is released, and button A of the IR remote is pressed, all LEDs will light up red.

---

## 10. () or ()

If either one of the two conditions is met, the report condition is met.

![](images/operators-10-1.png)

**Example:**

![](images/operators-10-2.png)

When the greed flag is clicked, if either the on-board button is pressed, or button A of the IR remote is pressed, all LEDs will light up red.

---

## 11. not ()

The report condition is met when the specified condition is not met.

![](images/operators-11-1.png)

**Example:**

![](images/operators-11-2.png)

When the greed flag is clicked, if the on-board button is not pressed, all LEDs will light up red.

---

## 12. join () ()

Join two specified character strings.

![](images/operators-12-1.png)

**Example:**

![](images/operators-12-2.png)

When the space key is pressed, the external LED panel will display "hi" and "morning" together.

---

## 13. letter () of ()

Report the letter at specified position of a character string.

![](images/operators-13-1.png)

**Example:**

![](images/operators-13-2.png)

When the space key is pressed, the external LED panel will display the third letter of "morning".

---

## 14. length of ()

Report the length of a specified character string.

![](images/operators-14-1.png)

**Example:**

![](images/operators-14-2.png)

When the space key is pressed, the external LED panel will display the length of "morning."

---

## 15. () contains ()?

If the specified character string contains the other specified character string, the report condition is met.

![](images/operators-15-1.png)

**Example:**

![](images/operators-15-2.png)

When the green flag is clicked, if "apple" contains "a", all LEDs will light up red.

---

## 16. () mod ()

Calculate the remainder (modular) of two specified numbers.

![](images/operators-16-1.png)

**Example:**

![](images/operators-16-2.png)

When the space key is pressed, the external LED panel will display the remainder of "9 &divide; 6".

---

## 17. round ()

Round the specified number to nearest integer.

![](images/operators-17-1.png)

**Example:**

![](images/operators-17-2.png)

When the space key is pressed, the external LED panel will display the rounded value of 10.7.

---

## 18. (abs) ()

Perform specific mathematical operation on the specified number. Mathematical operations include: abs (absolute value), floor, ceiling, sqrt (square root), sin, sos, tan, asin, atan, acos, ln, log, e^, and 10^.

![](images/operators-18-1.png)

**Example:**

![](images/operators-18-2.png)

When the space key is pressed, the external LED panel will display the square root of 16.

---