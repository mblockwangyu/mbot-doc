# Block Reference

This section includes detailed introduction of each block category.

* [Looks](looks.md)
* [Show](show.md)
* [Action](Action.md)
* [Sensing](sensing.md)
* [Events](events.md)
* [Control](control.md)
* [Operators](operators.md)