# Sensing

## 1. light sensor (on-board) light intensity

Reports light intensity by the specified light sensor.

![](images/sensing-1-1.png)

**Example:**

![](images/sensing-1-2.png)

When the space key is pressed, the light intensity value will be displayed on the external LED panel.

---

## 2. ultrasonic sensor (port3) distance cm

Reports the distance of obstacles detected by the ultrasonic sensor that is connected to the specified port.

![](images/sensing-2-1.png)

**Example:**

![](images/sensing-2-2.png)

When the space key is pressed, the distance of obstacles detected by the ultrasonic sensor will be displayed on the external LED panel.

---

## 3. line follower sensor (port2) value

Reports the value detected by the specified line follower sensor.

![](images/sensing-3-1.png)

**Example:**

![](images/sensing-3-2.png)

When the space key is pressed, the value detected by the line follower sensor will be displayed on the external LED panel.

---

## 4. line follower sensor (port2) detects (leftside) being(black)?

If the color detected by the specified line follower sensor on the specified side is the specified color, the report condition is met.

![](images/sensing-4-1.png)

**Example:**

![](images/sensing-4-2.png)

When the green flag is clicked, if the line follower sensor detects black obstacles on the left side, mBot will stop moving.

---

## 5. when on-board button (pressed)?

If the on-board button is pressed or released, the report condition is met.

![](images/sensing-5-1.png)

**Example:**

![](images/sensing-5-2.png)

When the green flag is clicked, if the on-board button is pressed, the LEDs of mBot will light up red.

---


## 6. IR remote (A) pressed?

If the specified button of the IR remote is pressed, the report condition is met.

![](images/sensing-6-1.png)

**Example:**

![](images/sensing-6-2.png)

When the green flag is clicked, if button A of the IR remote is pressed, "yes" will be displayed on the external LED panel.

---


## 7. send IR message (hello)

Sends the specified IR message.

![](images/sensing-7-1.png)

**Example:**

![](images/sensing-7-2.png)

When the space key is pressed, IR message "hello" will be sent out.

---

## 8. IR message received

Reports the received IR message.

![](images/sensing-8-1.png)

**Example:**

![](images/sensing-8-2.png)

When the space key is pressed, the IR message received will be displayed on the external LED panel.

---


## 9. timer

Reports the value of the timer.

![](images/sensing-9-1.png)

**Example:**

![](images/sensing-9-2.png)

When the space key is pressed, the value of the timer will be displayed on the external LED panel.

---


## 10. reset timer

Resets the timer.

![](images/sensing-10-1.png)

**Example:**

![](images/sensing-10-2.png)

When the space key is pressed, the timer will be reset.

---
