# Action

## 1. move forward at power (50)% for () secs

Moves mBot forward at the specified power for the specified amount of time.

![](images/action-1-1.png)

**Example:**

![](images/action-1-2.png)

When the "&uarr;" key is pressed, mBot will move forward at 50% power for 1 second.

---

## 2. move backward at power (50)% for () secs

Moves mBot backward at the specified power for the specified amount of time.

![](images/action-2-1.png)

**Example:**

![](images/action-2-2.png)

When the "&darr;" key is pressed, mBot will move backward at 50% power for 1 second.

---

## 3. turn left at power (50)% for () secs

Turns mBot left at the specified power for the specified amount of time.

![](images/action-3-1.png)

**Example:**

![](images/action-3-2.png)

When the "&larr;" key is pressed, mBot will turn left at 50% power for 1 second.

---

## 4. turn right at power (50)% for () secs

Turns mBot right at the specified power for the specified amount of time.

![](images/action-4-1.png)

**Example:**

![](images/action-4-2.png)

When the "&rarr;" key is pressed, mBot will turn right at 50% power for 1 second.

---

## 5. (move forward) at power (50)%

Moves mBot to the specified direction at the specified power. There are four options: "move forward", "move backward", "turn left", or "turn right".

![](images/action-5-1.png)

**Example:**

![](images/action-5-2.png)

When the space key is pressed, mBot will keep moving forward at 50% power.

---

## 6. left wheel turns at power (50)%, right wheel at power (50)%

Turns the left wheel and right wheel at the specified power.

![](images/action-6-1.png)

**Example:**

![](images/action-6-2.png)

When the space key is pressed, both the left wheel and the right wheel will move at 50% power.

---

## 7. stop moving

Makes mBot stop moving.

![](images/action-7-1.png)

**Example:**

![](images/action-7-2.png)

When the space key is pressed, mBot will stop moving.

---
