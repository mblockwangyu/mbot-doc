# Looks

<img src="images/led-panel.jpg" width="300px;" style="padding: 5px 5px 15px 5px;">

Add a LED matrix display to mBot to make it cuter and more playable.

Use a RJ25 cable to connect the LED matrix to a RJ25 port of the main board.

**Coordinate of the Face Panel**

<img src="images/led-matrix.png" width="400px;" style="padding: 5px 5px 15px 5px;">

As shown in the figure above, the face panel has the upper-left corner as the origin of the coordinate system, and the direction of x and y is indicated by the arrow. Parameters:
- x: -15 ~ 15
- y: -7 ~ 7

## 1. LED panel (port1) shows image () for () secs

Displays the specified image on mBot's screen that is connected to the specified port of the main board for the specified amount of seconds.

![](images/looks-1-1.png)

**Example:**

![](images/looks-1-2.png)

When the space key is pressed, the LED panel connected to port1 will display the specified image for 1 second.

---

## 2. LED panel (port1) shows image ()

Displays the specified image on mBot's screen that is connected to the specified port of the main board.

![](images/looks-2-1.png)

**Example:**

![](images/looks-2-2.png)

When the space key is pressed, the LED panel connected to port1 will switch between the two specified images for 10 times, with the time gap of 1 second.

---

## 3. LED panel (port1) shows image () at x:() y:()

Displays the specified image on mBot's screen that is connected to the specified port of the main board at the specified position.

![](images/looks-3-1.png)

**Example:**

![](images/looks-3-2.png)

When the space key is pressed, the LED panel connected to port1 will display the specified image at (0,0).

---

## 4. LED panel (port1) shows text ()

Displays the specified text on mBot's screen that is connected to the specified port of the main board.

![](images/looks-4-1.png)

**Example:**

![](images/looks-4-2.png)

When the space key is pressed, the LED panel connected to port1 will display "hello".

---

## 5. LED panel (port1) shows text () at x:() y:()

Displays the specified text on mBot's screen that is connected to the specified port of the main board at the specified position.

![](images/looks-5-1.png)

**Example:**

![](images/looks-5-2.png)

When the space key is pressed, the LED panel connected to port1 will display "hello" at (0,0).

---

## 6. LED panel (port1) shows number ()

Displays the specified number on mBot's screen that is connected to the specified port of the main board.

![](images/looks-6-1.png)

**Example:**

![](images/looks-6-2.png)

When the space key is pressed, the LED panel connected to port1 will display "2019".

---

## 7.  LED panel (port1) shows time ():()

Displays the specified time on mBot's screen that is connected to the specified port of the main board.

![](images/looks-7-1.png)

**Example:**

![](images/looks-7-2.png)

When the space key is pressed, the LED panel connected to port1 will display time "15:30".

---

## 8. LED panel (port1) clears screen

Clears the screen that is connected to the specified port of the main board.

![](images/looks-8-1.png)

**Example:**

![](images/looks-8-2.png)

When the space key is pressed, the content on mBot's external screen will be cleared.

---