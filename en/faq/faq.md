<img src='../../zh/faq/images/mbot.jpg' width='300px;'>

# FAQ for mBot

## 1. Why the Ultrasonic Obstacle Avoidance Mode doesn't work on mBot?

If you find your mBot cannot avoid obstacle when you set it in Ultrasonic Obstacle
Avoidance mode, please follow below steps to do troubleshooting.

1. Make sure the ultrasonic module is connected to Port 3 (required by default program).
2. Double check the wiring of the ultrasonic module and make sure the connection is firm
and well. Check whether the red indicator of the ultrasonic module is on. If not, the
problem is caused by wiring.
3. Press button B on the IR remote control to enter Ultrasonic Obstacle Avoidance Mode
if you use the IR remote controller. When it set in Ultrasonic Obstacle Mode, onboard light
should be green on.
4. Make sure the battery on mBot can provide enough power. According to our test and research, it is suggested to use rechargeable Li-ion battery or rechargeable nickel-metal hydride, nickel-cadmium which can be bought from amazon or
local shop. Or Alkaline battery with good quality like Energizer, DURACELL.

---

## 2. Why the Line Follower Mode doesn't work on mBot?

When mBot works properly with Manual mode and Wall avoidance mode but doesn’t work properly with Line follower mode, please test if the line follower sensor is faulty referring to the below steps.

### Step 1

Put the two sensors of the Me Line follower above (Detection range: 1~2cm) a white desk
or white paper and check if the two LEDs corresponding to the two sensors lit up?

<img src="images/faq-line-following-1.png" width="600px;" style="padding:5px 5px 15px 5px;">

### Step 2

Move it away from the white desk or white paper and check if the two LEDs corresponding
to the two sensors turn off?

<img src="images/faq-line-following-2.png" width="300px;" style="padding:5px 5px 15px 5px;">

If the LED corresponding to each sensor lights up when you put the sensor above a white paper/desk and goes off when it is away from white paper/desk, which means the line follower sensor is ok. Otherwise, the sensor should be faulty.

If the sensor is ok according to the above test but the line follower mode doesn’t work,
there are several possible reasons that we should check.

1. Make sure you have pressed **Button C** to enter Line Follower Mode if you use the IR
remote controller to control the mBot.<br>
<img src="images/modes-3.png" width="600px;" style="padding:5px 5px 15px 5px;">

2. The Me Line Follower sensor may not be connected to the correct RJ25 port. Please
connect the Line follower sensor to the Port 2 on mCore board.<br>
<img src="images/faq-line-following-3.png" width="600px;" style="padding:5px 5px 15px 5px;">

3. The wire connection may be loose or the RJ25 cable between the line follower module to mCore may faulty. Please **re-plug the RJ25 cable** for both sides, or **change a RJ25 cable** to have a check. 
4. The power from **battery** is not enough. Please change the battery have a check. 
According to our test and research, it is suggested to use rechargeable Li-ion battery or rechargeable nickel-metal hydride, nickel-cadmium which can be bought from amazon or local shop. Or  Alkaline battery with good quality like Energizer, DURACELL.

---

## 3. How to replace motor shaft for mBot?

There is spare motor shaft in the mBot package, please find it and refer to the [video](https://www.youtube.com/embed/MDb1uWpbK6Y)
tutorial to replace it if your shaft is broken.

---

## 4. How to reset default program and update the firmware of mBot in mBlock5?

### step1 
Download the latest mBlock software latest version in this [link](http://www.mblock.cc/mblock-software/ )
Decompress the file after download and install mBock software on PC.

### step2
Connect mBot to PC with USB cable and turn on the mBot.

### step3
Open mBlock software, click on “+” and choose mBot from device library. By the way, Codey is the default device and it is ok if you just delete it. If you also see “download icon” against mBot, please download it and reopen the software and add mBot here.
Then click **Settings** > **Update Firmware** > select **online firmware** to update firmware or select **factory firmware** to reset default program.

<img src="images/update-1.png" width="300px;" style="padding:5px;"><span style="font-size:30px;color:lightblue;padding:10px;">&rarr;</span> <img src="images/update-2.png" width="300px;" style="padding:5px;">

<img src="images/reset-1.png" width="600px;">

--- 

## 5. When I controlled the mBot car to move forward (or right), it moved backwards (or left). How can I fix this?

Please try following ways to solve this issue:

1.	The wires of the motors may be connected reversed. Please reconnect and have a check.

2.	Change the battery for mBot since mBot may not function normal when no enough power provided.

> <small>According to our test and research, it is suggested to use rechargeable Li-ion battery or rechargeable nickel-metal hydride, nickel-cadmium which can be bought from amazon or local shop. Or Alkaline battery with good quality like Energizer, DURACELL.</small>

---

## 6. How to change the speed of mBot?

If you control the mBot through IR remote controller with the factory default program, the 1-9 keys on the IR remote control are set to adjust the speed of mBot. 1 for slowest and 9 for fastest.

<img src="images/modes-3.png" width="700px;" style="padding:5px 5px 15px 5px;">

---

## 7. Why my mBot doesn't move when I set motor speed to 50?

When we set the speed to 50, here the "50" is not the real speed value. The real running speed depends on the voltage and speed value.

Here we take mBot as an example:  
 The setting speed is 50, and the maximum speed is 255.  
 The battery is 3.7V, and it is fully charged.  
 The rated voltage is 6V forTT Geared Motor, and the no load speed: 200RPM±10.

<img src="images/speed-1.png" width="600px;" style="padding:5px 5px 15px 5px;">

Thus, setting speed to 50 might be too slow for TT Geared Motors to run. Try 100 above to have a look.


---

## 8. How to troubleshoot if my mBot is not connected to Makeblock App with Bluetooth?

If you find Bluetooth cannot be connected to Makeblock App, please refer to below chart to do some troubleshooting. 

<img src="images/bluetooth.png" width="800px;" style="padding:5px 5px 15px 5px;">

**Notes:**  
1. We can also try to reset default program and upgrade firmware with USB cable connection on PC mBlock first, and then connect Bluetooth to Makeblock App.
2. If there Bluetooth signal is not detected, please enable GPS on your device.

---








