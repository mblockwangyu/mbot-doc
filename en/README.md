# mBot Docs

\* If you have any technical problems, please contact: <support@makeblock.com>

Thank you for your interest in mBot!

The help documents include the following parts.

* [Tutorials](README.md)
    * [Introduction](tutorials/introduction.md)
    * [Building mBot](tutorials/building.md)
    * [Preset Modes](tutorials/preset-modes.md)
    * [Connect mBot](tutorials/connect.md)
    * [Get Started](tutorials/quick-start.md)
* [Block Reference](block-reference/block-reference.md)
    * [Looks](block-reference/looks.md)
    * [Show](block-reference/show.md)
    * [Action](block-reference/Action.md)
    * [Sensing](block-reference/sensing.md)
    * [Events](block-reference/events.md)
    * [Control](block-reference/control.md)
    * [Operators](block-reference/operators.md)
* [Expand mBot](expand-mbot/expand-mbot.md)
* [FAQ](faq/faq.md)